package ru.t1.kharitonova.tm.util;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner scanner = new Scanner(System.in);

    static String nextLine(){
        return scanner.nextLine();
    }

    static Integer nextNumber(){
        final String value = nextLine();
        return Integer.parseInt(value);
    }
}
