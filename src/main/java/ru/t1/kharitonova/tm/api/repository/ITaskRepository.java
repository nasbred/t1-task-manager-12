package ru.t1.kharitonova.tm.api.repository;

import ru.t1.kharitonova.tm.model.Task;

import java.util.List;

public interface ITaskRepository {
    void add(Task task);

    List<Task> findAll();

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Integer getSize();
}
