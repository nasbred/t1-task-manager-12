package ru.t1.kharitonova.tm.api.service;

import ru.t1.kharitonova.tm.api.repository.ITaskRepository;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.model.Project;
import ru.t1.kharitonova.tm.model.Task;

import java.util.List;

public interface ITaskService {
    void add(Task task);

    List<Task> findAll();

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task create(String name, String description);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task changeTaskStatusById(String id, Status status);
}
