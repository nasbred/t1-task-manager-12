package ru.t1.kharitonova.tm.api.service;

import ru.t1.kharitonova.tm.api.repository.IProjectRepository;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.model.Project;

import java.util.List;

public interface IProjectService {
    void add(Project project);

    List<Project> findAll();

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project create(String name, String description);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project changeProjectStatusById(String id, Status status);
}
